package co.edu.udistrital.judcuervor.Catastro;

/**
 * @version 1.0
 * @author estudiantes
 * 
 */
public class Predio {
    
    protected String cedulaCatastral;

    public Predio(String cedulaCatastral) {
        this.cedulaCatastral = cedulaCatastral;
    }
    

    public String getCedulaCatastral() {
        return cedulaCatastral;
    }
    /**
     * Para verificar la consistencia de lso datos de entrada
     * @param cedulaCatastral 
     */

    public void setCedulaCatastral(String cedulaCatastral) {
        this.cedulaCatastral = cedulaCatastral;
    }
    
    public double calcularImpuesto(int indice){
        
        return 5000000*indice; 
    }
     public double calcularImpuesto(int indice,boolean exenciones){
        
        return 5000000*indice-0.2*50000000; 
    }
    public Predio() {
        
    }
    
    
    
}
